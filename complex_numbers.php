<?php
class ComplexNumbers {

	function add($number_1, $number_2) {
		// a + b
		$a = $number_1->a;
		$b = $number_1->b;
		$c = $number_2->a;
		$d = $number_2->b;
		$new_b = $b + $d;
		$plus_sign = "+";
		if ($new_b < 0) {
			$plus_sign = "";
		}
		$result = ($a + $c) . $plus_sign . $new_b . "i";
		echo "a+b:" . $result;
		echo "\n";
	}

	function subtract($number_1, $number_2) {
		// a - b
		$a = $number_1->a;
		$b = $number_1->b;
		$c = $number_2->a;
		$d = $number_2->b;
		$new_b = $b - $d;
		$plus_sign = "+";
		if ($new_b < 0) {
			$plus_sign = "";
		}
		$result = ($a - $c) . $plus_sign . $new_b . "i";
		echo "a-b:" . $result;
		echo "\n";
	}

	function multiply($number_1, $number_2) {
		// a * b
		$a = $number_1->a;
		$b = $number_1->b;
		$c = $number_2->a;
		$d = $number_2->b;
		$new_b = $a * $d + $b * $c;
		$plus_sign = "+";
		if ($new_b < 0) {
			$plus_sign = "";
		}
		$result = ($a * $c - $b * $d) . $plus_sign . $new_b . "i";
		echo "a*b:" . $result;
		echo "\n";
	}

	function divide($number_1, $number_2) {
		// a / b
		$a = $number_1->a;
		$b = $number_1->b;
		$c = $number_2->a;
		$d = $number_2->b;
		$new_b_numerator = $b * $c - $a * $d;
		$new_b_denominator = $c * $c + $d * $d;
		$plus_sign = "+";
		if ($new_b_numerator / $new_b_denominator < 0) {
			$plus_sign = "";
		}
		$result = ($a * $c + $b * $d) . "/" . (($c * $c) + ($d * $d)) . $plus_sign . $new_b_numerator . "/" . $new_b_denominator . "i ";
		echo "a/b:" . $result;
		echo "\n";
	}
}

class ComplexNumber {
	public $a = 0;
	public $b = 0;
}

for ($i = 0;$i < 5;$i++) {
	$min = - 10;
	$max = 10;
	$a = new ComplexNumber;
	$a->a = random_int($min, $max);
	$a->b = random_int($min, $max);

	$b = new ComplexNumber;
	$b->a = random_int($min, $max);
	$b->b = random_int($min, $max);
	$new_b = $a->b;
	$plus_sign = "+";
	if ($new_b < 0) {
		$plus_sign = "";
	}
	echo "a:" . $a->a . $plus_sign . $a->b . "i" . "\n";
	$new_b = $b->b;
	$plus_sign = "+";
	if ($new_b < 0) {
		$plus_sign = "";
	}
	echo "b:" . $b->a . $plus_sign . $b->b . "i" . "\n";;
	ComplexNumbers::add($a, $b);
	ComplexNumbers::subtract($a, $b);
	ComplexNumbers::multiply($a, $b);
	ComplexNumbers::divide($a, $b);
	echo "____________________________" . "\n";
}